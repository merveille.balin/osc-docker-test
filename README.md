# OSC

This project is based on the official Docker images from :
 * Elasticsearch
 * Kibana
 * Tomcat/ Java spring / Angular
 * PhpMyadmin
 * Mysql
 * Jenkins

## Requirements
    - Docker Engine version 17.05+
    - Docker Compose version 1.12.0+
    - 8 GB of RAM min

## Usage
Clone this repository onto the Docker host that will run the stack, then start services locally using Docker Compose:

```$ docker-compose up```

You can also run all services in the background (detached mode) by adding the "-d" flag to the above command.

```$ docker-compose up -d```

To see the current process use this command:

```$ docker-compose ps```

To see the current containers logs :

```$ docker-compose logs -f```

To use a specific container :

```$ winpty docker exec -it {" id of the container"} bash ```

To transfer data between host and container :

```docker cp <containerId>:/file/path/within/container /host/path/target```

## Cleanup

In order to entirely shutdown the stack and remove all persisted data, use the following Docker Compose command:

```$ docker-Compose down -v```

-------------------------------------------------
Jean.balin@capgemini.com

22/01/2020
